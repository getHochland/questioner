import Vuex from 'vuex'

export default new Vuex.Store({
    state:{
        directories:[
            {
                id: 1,
                type: 'folder',
                name: 'Dir-1',
                open: false,
                renameOn: false,
                inner:[
                    {
                        id: 11,
                        type: 'folder',
                        name: 'Dir-1-1',
                        open: false,
                        renameOn: false,
                        inner:[
                            {
                                id: 111,
                                type: 'file',
                                name: 'File-1-1-1',
                                renameOn: false,
                            }
                        ]
                    },
                    {
                        id: 12,
                        type: 'file',
                        name: 'File-1-2',
                        renameOn: false,
                    }
                ]
            },
            {
                id: 2,
                type: 'folder',
                name: 'Dir-2',
                open: false,
                renameOn: false,
                inner:[
                    {
                        id: 21,
                        type: 'folder',
                        name: 'Dir-2-1',
                        open: false,
                        renameOn: false,
                        inner:[]
                    },
                    {
                        id: 22,
                        type: 'file',
                        name: 'File-2-2',
                        renameOn: false,
                    }
                ]
            },
            {
                id: 3,
                type: 'file',
                name: 'File-2',
                renameOn: false,
            }
        ],
    },
    getters:{
        directories(state){
            return state.directories
        },
    },
    mutations: {
        OPEN_FOLDER(state, id) {
            // Проходимся по внешнему и внутренним массивам в поисках айдишника)
            state.directories.forEach(function (item) {
                if (item.id === id) {
                    item.open = !item.open
                }
                else {
                    if (item.type === 'folder' && item.inner.length > 0){
                        item.inner.forEach(function (item) {
                            if (item.id === id) {
                                item.open = !item.open
                            }
                        })
                    }
                }
            })
        },
        DELETE_ITEM(state, id){
            //Поиск и удаление элемента из массива
            state.directories.forEach(function (item, index) {
                if (item.id === id) {
                    state.directories.splice(index, 1)
                }
                else {
                    if (item.type === 'folder' && item.inner.length > 0){
                        item.inner.forEach(function (item, index, array) {
                            if (item.id === id) {
                                array.splice(index, 1)
                            }
                            else {
                                if (item.type === 'folder' && item.inner.length > 0){
                                    item.inner.forEach(function (item, index, array) {
                                        if (item.id === id) {
                                            array.splice(index, 1)
                                        }
                                    })
                                }
                            }
                        })
                    }
                }
            })
        }
    }
})